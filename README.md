# Catie Exercises

In this repository are two exercises:
* Radar
* Survey website

I chose to do the radar as I'm more comfortable with the technologies used. I'm currently learning React/Ruby/Sinatra, however, at my current understanding I don't think 4 hours would have sufficed for the Survey website.

Notes have been included to how I would build the Survey website.