# Exercise 2 - Survey website

##### Technologies I would use:
* Frontend: React.js with preferred front-end library (Bootstrap preferred)
* Backend: Ruby/Sinatra
* Database: Either SQL or NoSQL depending on preference (NoSQL preferred)


Using the [react-survey](https://github.com/aisensiy/react-survey) library, we can quickly implement a survey system that includes the user survey page and an admin page to create a new survey or view metrics. Using some CSS and Bootstrap to design the view to our liking.

The react-survey GitHub repository includes a premade backend in Rails 5.

Reimplementing the backend with Sinatra and adapting the database to the preferred choice wouldn't be too time consuming.