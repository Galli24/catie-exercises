# Exercise 1 - Radar

* Framework: Unity 3D (version 2019.4.7f1)
* Language: C#
* Time taken: 2 hours 37 minutes

The program contains two grids, one labeled "Input" and the other "Output".

Two buttons are available to add a car or pedestrian. Cars and pedestrians can be moved by dragging them by holding the left mouse button over the object. They can also be removed with the right mouse button.

By moving a car or pedestrian around in the Input grid, the Radar will detect them and display the object's name on the output grid.