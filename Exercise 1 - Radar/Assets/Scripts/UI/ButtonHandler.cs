﻿using UnityEngine;

public class ButtonHandler : MonoBehaviour
{
    public Canvas Canvas;
    public GameObject Board;
    public GameObject Car;
    public GameObject Pedestrian;

    public void AddCar()
    {
        Instantiate(Car, Board.transform.position, Quaternion.identity, Canvas.transform);
    }

    public void AddPedestrian()
    {
        Instantiate(Pedestrian, Board.transform.position, Quaternion.identity, Canvas.transform);
    }
}
