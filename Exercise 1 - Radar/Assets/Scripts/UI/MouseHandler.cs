﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseHandler : MonoBehaviour
{
    void Start()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();

        EventTrigger.Entry pointerClickEntry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.PointerClick
        };
        pointerClickEntry.callback.AddListener((data) => { OnPointerClick((PointerEventData)data); });

        EventTrigger.Entry dragEntry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Drag
        };
        dragEntry.callback.AddListener((data) => { OnDragDelegate((PointerEventData)data); });

        trigger.triggers.Add(pointerClickEntry);
        trigger.triggers.Add(dragEntry);
    }

    public void OnPointerClick(PointerEventData data)
    {
        if (data.button == PointerEventData.InputButton.Right)
            Destroy(gameObject);
    }

    public void OnDragDelegate(PointerEventData data)
    {
        transform.position += new Vector3(data.delta.x, data.delta.y, 0) / transform.lossyScale.x;
    }
}
