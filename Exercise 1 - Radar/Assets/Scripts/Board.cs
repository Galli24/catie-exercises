﻿using UnityEngine;

public class Board : MonoBehaviour
{
    public LayerMask m_LayerMask;

    public delegate void CollidedObjectDelegate(Collider2D[] colliders);
    public CollidedObjectDelegate OnCollidedObjects;

    void FixedUpdate()
    {
        CheckForBoardCollision();
    }

    void CheckForBoardCollision()
    {
        var hitColliders = Physics2D.OverlapBoxAll(transform.position, ((RectTransform)transform).rect.size, 0f, m_LayerMask, 0);
        OnCollidedObjects(hitColliders);
    }
}