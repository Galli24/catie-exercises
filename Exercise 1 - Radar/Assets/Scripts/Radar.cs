﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Radar : MonoBehaviour
{
    public GameObject InputBoard;
    public GameObject OutputBoard;
    public Dictionary<GameObject, Quadrants> _radarData;

    public enum Quadrants
    {
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT,
        LEFT,
        RIGHT,
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT
    }

    private Dictionary<Quadrants, Text> _outputs;

    private float _gridSplitCoord = 85.3f;

    void Start()
    {
        var board = InputBoard.GetComponent<Board>();
        board.OnCollidedObjects += GetEntitiesOnRadar;

        _radarData = new Dictionary<GameObject, Quadrants>();
        _outputs = new Dictionary<Quadrants, Text>();

        var textObjects = OutputBoard.GetComponentsInChildren<Text>();
        foreach (var textObject in textObjects)
        {
            switch (textObject.name)
            {
                case "BottomLeftText":
                    _outputs.Add(Quadrants.BOTTOM_LEFT, textObject);
                    break;
                case "BottomCenterText":
                    _outputs.Add(Quadrants.BOTTOM_CENTER, textObject);
                    break;
                case "BottomRightText":
                    _outputs.Add(Quadrants.BOTTOM_RIGHT, textObject);
                    break;
                case "LeftText":
                    _outputs.Add(Quadrants.LEFT, textObject);
                    break;
                case "RightText":
                    _outputs.Add(Quadrants.RIGHT, textObject);
                    break;
                case "TopLeftText":
                    _outputs.Add(Quadrants.TOP_LEFT, textObject);
                    break;
                case "TopCenterText":
                    _outputs.Add(Quadrants.TOP_CENTER, textObject);
                    break;
                case "TopRightText":
                    _outputs.Add(Quadrants.TOP_RIGHT, textObject);
                    break;
            }
        }
    }

    void GetEntitiesOnRadar(Collider2D[] colliders)
    {
        var gameObjects = new List<GameObject>();
        foreach (var collider in colliders)
        {
            if (collider.gameObject.CompareTag("Car") || collider.gameObject.CompareTag("Pedestrian"))
                gameObjects.Add(collider.gameObject);
        }

        _radarData.Clear();
        if (gameObjects.Count > 0)
        {
            foreach (var go in gameObjects)
            {
                var vector = (go.transform.position - transform.position);
                if (Mathf.Abs(vector.x) < ((RectTransform)transform).rect.width / 2 && Mathf.Abs(vector.y) < ((RectTransform)transform).rect.width / 2)
                    continue;

                Debug.Log(go.name + " - " + vector);

                if (vector.y >= _gridSplitCoord * -1 && vector.y <= _gridSplitCoord)
                {
                    if (vector.x <= _gridSplitCoord)
                        _radarData.Add(go, Quadrants.LEFT);
                    else if (vector.x >= _gridSplitCoord)
                        _radarData.Add(go, Quadrants.RIGHT);
                }
                else if (vector.y <= _gridSplitCoord * -1)
                {
                    if (vector.x <= _gridSplitCoord * -1)
                        _radarData.Add(go, Quadrants.BOTTOM_LEFT);
                    else if (vector.x >= _gridSplitCoord)
                        _radarData.Add(go, Quadrants.BOTTOM_RIGHT);
                    else
                        _radarData.Add(go, Quadrants.BOTTOM_CENTER);
                }
                else if (vector.y >= _gridSplitCoord)
                {
                    if (vector.x <= _gridSplitCoord * -1)
                        _radarData.Add(go, Quadrants.TOP_LEFT);
                    else if (vector.x >= _gridSplitCoord)
                        _radarData.Add(go, Quadrants.TOP_RIGHT);
                    else
                        _radarData.Add(go, Quadrants.TOP_CENTER);
                }
            }
        }

        UpdateRadar();
    }

    void UpdateRadar()
    {
        foreach (var output in _outputs)
        {
            output.Value.text = "";
        }

        foreach (var data in _radarData)
        {
            _outputs[data.Value].text += $"{data.Key.tag}\n";
        }
    }
}
